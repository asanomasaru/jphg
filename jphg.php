<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>aquagarage スタッフ試着感想</title>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
	body{
		background-color:beige;
	}
	p{
		width:100%;
	}
	img{
		max-width:700px;
		width:100%;
	}
</style>
</head>
<body>
<?PHP
//rakuten FTP info
//host : upload.rakuten.ne.jp
//ID : garageshop 
// pass:IIssEE61

$mode='test';//test or honban
$status='check';//check or upload
	
$currentURL="https://".$_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"];
$reqID=$_GET['id'];
if($_GET['st']){$status=$_GET['st'];};
if($_GET['mode']){$mode=$_GET['mode'];};
	
echobr($reqID.' / '.$status.' / '.$mode);

//pmg spreadsheet URL
//https://docs.google.com/spreadsheets/d/1Fuj2jqmivvLKHtVASibaUfLBkInNL_j3KD_y6-okMlU/edit?usp=sharing
//https://spreadsheets.google.com/feeds/list/{ここがIDです！}/od6/public/values?alt=json
//本番用
//https://docs.google.com/spreadsheets/d/1py7TwrjZDTfyV5cW7ud6I2lpnq2OGt7An0LSRc6nFYo/edit#gid=0
$spreadsheetID='1py7TwrjZDTfyV5cW7ud6I2lpnq2OGt7An0LSRc6nFYo';
$spreadsheetJson='https://spreadsheets.google.com/feeds/list/'.$spreadsheetID.'/od6/public/values?alt=json';
if(!$reqID){
	echobr('<a href="https://docs.google.com/spreadsheets/d/'.$spreadsheetID.'/edit#gid=0">spreadsheet</a>');
}

$json=file_get_contents($spreadsheetJson);
$json=mb_convert_encoding($json,'UTF8','ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
$arr=json_decode($json,true);
	
//spreadsheet 2ページ目以降 取得方法
//http://creator.aainc.co.jp/archives/6240
$spreadsheetStaffNameKey='og8x2sq';
$spreadsheetStaffNameURL='https://spreadsheets.google.com/feeds/list/'.$spreadsheetID.'/'.$spreadsheetStaffNameKey.'/public/values?alt=json';
$staffNameJson=file_get_contents($spreadsheetStaffNameURL);
$staffNameJson=mb_convert_encoding($staffNameJson,'UTF8','ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
$staffNameJsonArr=json_decode($staffNameJson,true);
//print_r($staffNameJsonArr);
$nameArr=array();
foreach($staffNameJsonArr['feed']['entry'] as $staffNameObj){
	$nameArr[$staffNameObj['gsx$name']['$t']]=$staffNameObj['gsx$id']['$t'];
};
//print_r($nameArr);
$arr=$arr['feed']['entry'];//情報配列
$personArr=array();
//$nameArr=array('川辺'=>'bechan','佐藤'=>'chiitan','波多野'=>'kirari','小松'=>'kudo','南後'=>'nanchan','稲垣'=>'riri','いない'=>'sachi','白浜'=>'shiyan','通堂'=>'yuki');
//************************************ person作成 ************************************************************************************************************************************************
foreach($arr as $key=>$val){
	$id=$val['gsx$id']['$t'];
	//echo '<p>'.$reqID.' / '.$id.' / '.$size.' / '.$name.' / '.$message.'</p>';<br>
	//$personArr=array(array('id'=>'mw00080','name'=>'masaru','size'=>'XL','trySize'=>'L','text'=>'bbbbbbbbbb','imgUrl'=>'http://hoge.jpg'),array('name'=>'masaru','size'=>'XL','text'=>'bbbbbbbbbb','imgUrl'=>'http://hoge2.jpg'));
	if($reqID==$id){
		$type=mb_convert_encoding($val['gsx$type']['$t'],'UTF8');
		if($type=='靴'){$type='shoes_';}else if($type=='服'){$type='wear_';};
		$nameKey=mb_convert_encoding($val['gsx$name']['$t'],'UTF8');
		$name=$nameArr[$nameKey];
		$size=mb_convert_encoding($val['gsx$size']['$t'],'UTF8');
		$trySize=mb_convert_encoding($val['gsx$trysize']['$t'],'UTF8');
		$tryImgUrl=mb_convert_encoding($val['gsx$tryimgurl']['$t'],'UTF8');
		$text=mb_convert_encoding($val['gsx$message']['$t'],'UTF8');
		$person=array();
		if(!$tryImgUrl){
			$brCount=37;
			$person['tryImg']='false';
		}else if($tryImgUrl){
			$brCount=20;
			$person['tryImg']='true';
		};
		$person['brCount']=$brCount;
		$textRowCount=ceil(mb_strlen($text)/$brCount);
		$person['id']=$id;
		$person['type']=$type;
		$person['name']=$name;
		if($size){$person['size']=$size;};
		if($trySize){$person['trySize']=$trySize;};
		if($tryImgUrl){$person['tryImgUrl']=$tryImgUrl;};
		$person['text']=$text;
		$person['textRowCount']=$textRowCount;
		$person['size']=$size;
		$person['trySize']=$trySize;
		$personArr[]=$person;
	};
};
/*
echo '$personArr';
print_r($personArr);
*/

//************************************ 画像作成用の配列作成 ************************************************************************************************************************************************
$imgArr=array();
//************************************ 画像の基本情報 ************************************
$imgW=700;
$imgH=0;
$textFontSize=13.2;
$textLineHeight=1.3;
$margin=20;
//************************************ 画像を読み込んで高さを決定 ************************************
//************************************ head
$head_path='template/head.png';
$head_png=imagecreatefrompng($head_path);
$head_H=getimagesize($head_path)[1];
$imgArr[]=array('path'=>$head_path,'y'=>0,'H'=>$head_H,'png'=>$head_png);
$imgH=$head_H;
//************************************ 繰り返し部分
foreach($personArr as $index=>$person){
	//名前部分の高さ取得
	//************************************ name
	$name_path='template/'.$person['type'].$person['name'].'.png';
	$name_png=imagecreatefrompng($name_path);
	$name_H=getimagesize($name_path)[1];
	$imgArr[]=array('path'=>$name_path,'y'=>$imgH,'H'=>$name_H,'png'=>$name_png);
	$imgH+=$name_H;
	//サイズ部分の高さ取得
	//************************************ size
	if($person['size']||$person['trySize']){
		$size_path='template/size.png';
		$size_png=imagecreatefrompng($size_path);
		$size_H=getimagesize($size_path)[1];
		$imgArr[]=array('path'=>$size_path,'y'=>$imgH,'H'=>$size_H,'png'=>$size_png,'size'=>$person['size'],'trySize'=>$person['trySize']);
		$imgH+=$size_H;
	};
	$imgH+=$margin;
	if($person['tryImgUrl']){
		//試着画像のサイズ取得、テキストが画像サイズ以下だったら画像優先
		$tryImg_path=$person['tryImgUrl'];
		$tryImg=getimagesize($tryImg_path);
		$tryImg_W=$tryImg[0];
		$tryImg_H=$tryImg[1];
		$tryImg_type=$tryImg['mime'];
		if($tryImg_H>$person['textRowCount']*($textFontSize*$textLineHeight*1.4)){
			//画像Hの方がテキストHよりも高かったら
			$text_H=200;
		}else{
			//テキストHの方が画像Hより高かったら
			$text_H=$person['textRowCount']*($textFontSize*$textLineHeight*1.4);
		};
		$text_png=imagecreatetruecolor($imgW,$text_H);
		$white = imagecolorallocate($text_png, 255, 255, 255);
		imagefill($text_png,0,0,$white);
		/*
		echo '$tryImg_type : '.$tryImg_W.' / '.$tryImg_H.' / '.$tryImg_type;
		print_r($tryImg);
		*/
		if($tryImg_type=='image/jpeg'){
			$tryImg_png=imagecreatefromjpeg($tryImg_path);
		}else if($tryImg_type=='image/png'){
			$tryImg_png=imagecreatefrompng($tryImg_path);
		}else if($tryImg_type=='image/gif'){
			$tryImg_png=imagecreatefromgif($tryImg_path);
		};
		imagecopyresized($text_png,$tryImg_png,10,0,0,0,300,200,$tryImg_W,$tryImg_H);
	}else{
		//試着画像が無い場合はテキスト部分の高さ取得
		$text_H=$person['textRowCount']*($textFontSize*$textLineHeight*1.4);
		$text_png=imagecreatetruecolor($imgW,$text_H);
		$white = imagecolorallocate($text_png, 255, 255, 255);
		imagefill($text_png,0,0,$white);
	};
	$imgArr[]=array('y'=>$imgH,'H'=>$text_H,'png'=>$text_png,'text'=>$person['text'],'tryImg'=>$person['tryImg'],'brCount'=>$person['brCount']);
	$imgH+=$text_H;
	$imgH+=$margin;
};
/*
echo '$imgArr';
print_r($imgArr);
*/
//************************************ 画像を作成 ************************************************************************************************************************************************
$textFont = 'ipag.ttf';
$sizeFont='mplus-2c-bold.ttf';
$sizeFontSize='25';
$extrainfo = Array("linespacing" => $textLineHeight);
$stage=imagecreatetruecolor($imgW,$imgH);
$white = imagecolorallocate($stage, 255, 255, 255);
$lightgrey = imagecolorallocate($stage, 204, 204, 204);
$darkgrey = imagecolorallocate($stage, 51, 51, 51);
imagefill($stage,0,0,$white);
//************************************ 人ごとの部分画像作成 ************************************
foreach($imgArr as $parts){
	imagecopy($stage,$parts['png'],0,$parts['y'],0,0,$imgW,$parts['H']);
	//サイズ指定があったら
	if(array_key_exists('size',$parts)){
		imagefttext($stage,$sizeFontSize,0,190,$parts['y']+52,$lightgrey,$sizeFont,$parts['size']);
	};
	//試着サイズ指定があったら
	if(array_key_exists('trySize',$parts)){
		imagefttext($stage,$sizeFontSize,0,530,$parts['y']+52,$lightgrey,$sizeFont,$parts['trySize']);
	};
	//テキストがあったら
	if(array_key_exists('text',$parts)){
		$printText=insertStr($parts['text'],"\n",$parts['brCount']);
		if($parts['tryImg']=='true'){
			//着用画像があったら
			//imagefttext(描画対象, size, angle, x, y, color, font, text,extrainfo);
			imagefttext($stage,$textFontSize,0,320,$parts['y']+12,$darkgrey,$textFont,$printText,$extrainfo);
		}else{
			//着用画像が無かったら
			//imagefttext(描画対象, size, angle, x, y, color, font, text,extrainfo);
			imagefttext($stage,$textFontSize,0,15,$parts['y']+12,$darkgrey,$textFont,$printText,$extrainfo);
		}
	};
};
//画像書き出し
if($status=='check'){
	ob_start();
	imagejpeg($stage, null, 90);
	$sampleImg = base64_encode(ob_get_contents());
	ob_end_clean();
	imagedestroy($stage);
	echo '<p>file name : s-'.$reqID.'.jpg</p>';
	echo '<img src="data:image/jpeg;base64,'.$sampleImg.'" alt="sample" />';
	echo '<p>この画像で良いですか？</p>';
	echo '<p><a href="'.$currentURL.'?id='.$reqID.'&st=upload">はい</a> / <a href="#" onClick="window.close(); return false;">いいえ</a></p>';
};
	
	
//************************************ 画像をFTPで保存 ************************************************************************************************************************************************
$ftpInfoArr=array(
	'rakuten'=>array(
		'ftp_server'=>'upload.rakuten.ne.jp',
		'ftp_port'=>'21',
		'ftp_upload_dir'=>'cabinet/images',
		'temp_dir'=>'staff-sityaku',
		'ftp_user'=>'garageshop',
		'ftp_pass'=>'IIssEE61'
		),
	'wowma'=>array(
		'ftp_server'=>'bcimg3-origin.wowma.net',
		'ftp_port'=>'21',
		'ftp_upload_dir'=>'ext_cabinet',
		'temp_dir'=>'',
		'ftp_user'=>'u7299782',
		'ftp_pass'=>'w3k479y2'
		),
	'yahoo'=>array(
		'ftp_server'=>'ftp.castle.yahoofs.jp',
		'ftp_port'=>'21',
		'ftp_upload_dir'=>'',
		'temp_dir'=>'',
		'ftp_user'=>'store-rumsee',
		'ftp_pass'=>'Siwasu123'
		),
	'aquagarage'=>array(
		'ftp_server'=>'fsimg.aqua-garage.jp',
		'ftp_port'=>'21',
		'ftp_upload_dir'=>'public_html/fs2cabinet/staff',
		'temp_dir'=>'',
		'ftp_user'=>'aquagarage',
		'ftp_pass'=>'7fUqmZu1'
	)
);

if($status=='upload'){
	if($mode=='honban'){
		foreach($ftpInfoArr as $storeName=>$ftpInfo){
			saveImg($reqID,$stage,$storeName,$ftpInfo);
		};
	}else{
		saveImg($reqID,$stage,'sarustar',array('ftp_server'=>'ftp.dp43071603.lolipop.jp','ftp_port'=>'21','ftp_upload_dir'=>'_FILE/minako','temp_dir'=>'','ftp_user'=>'lolipop.jp-dp43071603','ftp_pass'=>'L53Z9QMi'));
	}
};
function saveImg($reqID,$im,$storeName,$ftpInfo){
	echobr($storeName.' -----------------------------------------------');
	echobr('start dir check');
	$ftp_server = $ftpInfo['ftp_server'];
	$ftp_port = $ftpInfo['ftp_port'];
	$ftp_upload_dir=$ftpInfo['ftp_upload_dir'];//必ずあるdir
	$dir=$ftpInfo['temp_dir'];//無いかもしれないdir
	$ftp_user = $ftpInfo['ftp_user'];
	$ftp_pass = $ftpInfo['ftp_pass'];
	//echobr('<img src="'.$file_root.'/'.$ftp_upload_dir.'/'.$dir.'/s-'.$reqID.'.jpg?v='.time().'"><br>s-'.$reqID.'.jpg');
	//ftp接続
	$ftp_conn=ftp_connect($ftp_server,$ftp_port) or die("Couldn't connect to $ftp_server");
	$ftp_root='ftp://'.$ftp_user.':'.$ftp_pass.'@'.$ftp_server;
	if($dir){$ftp_upload_dir=$ftp_upload_dir.'/';};
	$ftp_dir_absolute_path=$ftp_root.'/'.$ftp_upload_dir.$dir;
	$ftp_upload_img_path=$ftp_dir_absolute_path.'/s-'.$reqID.'.jpg';
	
	if(ftp_login($ftp_conn,$ftp_user,$ftp_pass)){//ftpログインを試みる
		echobr('ftp login success.');
		if(ftp_chdir($ftp_conn,$ftp_upload_dir)){//ディレクトリに移動
			echobr($ftp_upload_dir.' is ready.');
			if(file_exists($ftp_dir_absolute_path)){//temp_dirがあるかチェック
				echobr($ftp_dir_absolute_path.' is ready.');
				if(file_exists($ftp_upload_img_path)){//アップロードファイルがあるかチェック
					echobr($ftp_upload_img_path.' already saved image.');
					if(ftp_delete($ftp_conn,'/'.$ftp_upload_dir.$dir.'/s-'.$reqID.'.jpg')){
						echobr($ftp_upload_dir.$dir.'/s-'.$reqID.'.jpg'.' delete success');
					}else{
						echobr($ftp_upload_dir.$dir.'/s-'.$reqID.'.jpg'.' delete error');
					}
				}else{
					echobr('no s-'.$reqID.'.jpg');
				}
				//保存
				file_upload_to_ftp($ftp_conn,$storeName,$im,$ftp_dir_absolute_path,$reqID);
			}else{
			//ディレクトリがなければ作成
				echobr('no '.$dir);
				if(ftp_mkdir($ftp_conn,$dir)){
					echobr('make '.$dir);
					//ディレクトリ作成に成功したら保存
					file_upload_to_ftp($ftp_conn,$storeName,$im,$ftp_dir_absolute_path,$reqID);
				}
			}
		}
		
	}else{
		echobr('ftp login error');
	};
};

function file_upload_to_ftp($ftp_conn,$storeName,$im,$ftp_dir_absolute_path,$reqID){
	//upload対象ディレクトリに移動している状態
	echobr($storeName.' start file ftp upload.');
	//if($storeName=='yahoo'){
	if($storeName=='sarustar'){
		//jpg作成
		imagejpeg($im,'s-'.$reqID.'.jpg');
		//zip作成
		echobr('make zip');
		$zip=new ZipArchive();
		$zipName='lib_img'.date('YmdHis').'.zip';
		$res=$zip->open($zipName,ZipArchive::CREATE);
		//zipに格納
		$zip->addFile('s-'.$reqID.'.jpg');
		$zip->close();
		//jpg削除
		imagedestroy($im);
		if(unlink('s-'.$reqID.'.jpg')){
			echobr('s-'.$reqID.'.jpg'.' delete success');
		}else{
			echobr('s-'.$reqID.'.jpg'.' delete error ');
		};
		//指定フォルダに移動
		ftp_pasv($ftp_conn, true);
		if(ftp_put($ftp_conn,$zipName,$zipName,FTP_BINARY)){
			echobr('zip move success!');
		}else{
			echobr('zip move error...');
		}
	}
	else{
		echobr($storeName.' upload start.');
		imagejpeg($im,$ftp_dir_absolute_path.'/s-'.$reqID.'.jpg');
		imagedestroy($im);
		echobr('upload finish!!');
		ftp_close($ftp_conn);
	};
};

//************************************ 文字数で改行 ************************************************************************************************************************************************
function insertStr($text, $insert, $num){
    $returnText = $text;
    $text_len = mb_strlen($text, "utf-8");
    $insert_len = mb_strlen($insert, "utf-8");
    for($i=0; ($i+1)*$num<$text_len; $i++) {
        $current_num = $num+$i*($insert_len+$num);
        $returnText = preg_replace("/^.{0,$current_num}+\K/us", $insert, $returnText);
    }
    return $returnText;
}
//************************************ echobr ************************************************************************************************************************************************
function echobr($str){echo '<p>'.$str.'</p>';};
//************************************ log ************************************************************************************************************************************************
function sLog($str){
	
};
?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>